package no.accelerate.assignment2sample.runners;

import no.accelerate.assignment2sample.models.Customer;
import no.accelerate.assignment2sample.repositories.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Class that will run when the application runs.
 * It is used to showcase the functionality from the repositories.
 */
@Component
public class ChinookRunner implements CommandLineRunner {

    private final CustomerRepository customerRepository;

    public ChinookRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        // Requirement 1
        System.out.println("REQUIREMENT 1");
        System.out.println("=================================");
        customerRepository.findAll().forEach(System.out::println);
        System.out.println("=================================");
        // Requirement 2
        System.out.println("REQUIREMENT 2");
        System.out.println("=================================");
        System.out.println(customerRepository.findById(1));
        System.out.println("=================================");
        // Requirement 3
        System.out.println("REQUIREMENT 3");
        System.out.println("=================================");
        customerRepository.findByFirstOrLastName("T").forEach(System.out::println);
        System.out.println("=================================");
        // Requirement 4
        System.out.println("REQUIREMENT 4");
        System.out.println("=================================");
        customerRepository.getPage(10,20).forEach(System.out::println);
        System.out.println("=================================");
        // Requirement 5
        System.out.println("REQUIREMENT 5");
        System.out.println("=================================");
        Customer newCust = new Customer(0,"Nicholas","Lennox","RSA","12345","+47 421 00230","giga.chad@gmail.com");
        System.out.println(customerRepository.add(newCust));
        customerRepository.findByFirstOrLastName("Lennox").forEach(System.out::println);
        System.out.println("=================================");
        // Requirement 6
        System.out.println("REQUIREMENT 6");
        System.out.println("=================================");
        Customer existingCust = customerRepository.findById(19);
        Customer updatedCust = new Customer(
                existingCust.id(),
                existingCust.firstName(),
                "COOKED",
                existingCust.country(),
                existingCust.postalCode(),
                existingCust.phoneNumber(),
                existingCust.email());
        System.out.println(customerRepository.update(updatedCust));
        System.out.println(customerRepository.findById(19));
        System.out.println("=================================");
        // Requirement 7
        System.out.println("REQUIREMENT 7");
        System.out.println("=================================");
        System.out.println(customerRepository.getCountryWithMostCustomers());
        System.out.println("=================================");
        // Requirement 8
        System.out.println("REQUIREMENT 8");
        System.out.println("=================================");
        System.out.println(customerRepository.getHighestSpender());
        System.out.println("=================================");
        // Requirement 9
        System.out.println("REQUIREMENT 9");
        System.out.println("=================================");
        customerRepository.getMostPopularGenre(12).forEach(System.out::println);
        System.out.println("=================================");
    }
}
