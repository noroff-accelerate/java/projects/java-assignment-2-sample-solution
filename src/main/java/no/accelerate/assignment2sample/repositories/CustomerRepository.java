package no.accelerate.assignment2sample.repositories;

import no.accelerate.assignment2sample.models.Customer;
import no.accelerate.assignment2sample.models.CustomerCountry;
import no.accelerate.assignment2sample.models.CustomerGenre;
import no.accelerate.assignment2sample.models.CustomerSpender;

import java.util.Collection;

/**
 * Customer-specific repository that extends CRUD functionality.
 */
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    /**
     * Returns all the customers that match the search terms provided.
     * <br/><br/> Corresponds to Appendix B, Number 2, Requirement 3. <br/><br/>
     * @param search first name or last name to match.
     * @return Collection of customers matching search.
     */
    Collection<Customer> findByFirstOrLastName(String search);

    /**
     * Gets a page of customers from the database.
     * <br/><br/> Corresponds to Appendix B, Number 2, Requirement 4. <br/><br/>
     * @param limit How many to fetch.
     * @param offset Which row to start fetching from.
     * @return Collection of customers that are in the specified range.
     */
    Collection<Customer> getPage(int limit, int offset);

    /**
     * Gets the country with the most customers.
     * <br/><br/> Corresponds to Appendix B, Number 2, Requirement 7. <br/><br/>
     * @return Country name and customer count as CustomerCountry.
     */
    CustomerCountry getCountryWithMostCustomers();

    /**
     * Gets the customer who spent the most.
     * <br/><br/> Corresponds to Appendix B, Number 2, Requirement 8. <br/><br/>
     * @return Customer and the total amount they spent as CustomerSpender.
     */
    CustomerSpender getHighestSpender();

    /**
     * Gets the genres that are most frequently purchased by a customer.
     * <br/><br/> Corresponds to Appendix B, Number 2, Requirement 9. <br/><br/>
     * @param customerId
     * @return The customer, genre and count as CustomerGenre.
     */
    Collection<CustomerGenre> getMostPopularGenre(int customerId);
}
