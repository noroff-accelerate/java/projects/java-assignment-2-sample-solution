package no.accelerate.assignment2sample.repositories;

import no.accelerate.assignment2sample.models.Customer;
import no.accelerate.assignment2sample.models.CustomerCountry;
import no.accelerate.assignment2sample.models.CustomerGenre;
import no.accelerate.assignment2sample.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the CustomerRepository.
 * It is also tagged as a @Repository to be managed by Spring IoC.
 */
@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private String url;
    private String username;
    private String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * Gets all the customers in the database.
     * <br/><br/> Corresponds to Appendix B, Number 2, Requirement 1. <br/><br/>
     * @return Collection of Customers
     */
    @Override
    public Collection<Customer> findAll() {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email \n" +
                "FROM customer ";
        Set<Customer> customers = new HashSet<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            System.out.printf(e.getMessage());
        }
        return customers;
    }

    /**
     * Get a specific customer by their ID.
     * <br/><br/> Corresponds to Appendix B, Number 2, Requirement 2. <br/><br/>
     * @param id
     * @return Customer, if null, no customer was found.
     */
    @Override
    public Customer findById(Integer id) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email \n" +
                "FROM customer \n" +
                "WHERE customer_id = ?";

        Customer customer = null;

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            System.out.printf(e.getMessage());
        }
        return customer;
    }

    /**
     * Adds a new customer to the database.
     * <br/><br/> Corresponds to Appendix B, Number 2, Requirement 5. <br/><br/>
     * @param customer
     * @return number of rows affected. If this is 0, the insert failed.
     */
    @Override
    public int add(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) " +
                "VALUES (?,?,?,?,?,?)";
        int result = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;

    }

    /**
     * Updates an existing customer.
     * <br/><br/> Corresponds to Appendix B, Number 2, Requirement 6. <br/><br/>
     * @param customer
     * @return number of rows affected. If this is 0, the update failed.
     */
    @Override
    public int update(Customer customer) {
        String sql = "UPDATE customer SET first_name=?, last_name=?, country=?, postal_code=?, phone=?, email=? " +
                "WHERE customer_id=?";
        int result = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phoneNumber());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.id());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Collection<Customer> findByFirstOrLastName(String search) {
        String sql = "Select * \n" +
                "FROM customer \n" +
                "WHERE first_name LIKE ?\n" +
                "OR last_name LIKE ?";
        Set<Customer> customers = new HashSet<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            // To include the % we need to pass it here, otherwise the ? gets ignored
            statement.setString(1, "%" + search + "%");
            statement.setString(2, "%" + search + "%");
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                customers.add(new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                ));
            }
        } catch (SQLException e) {
            System.out.printf(e.getMessage());
        }
        return customers;
    }

    @Override
    public Collection<Customer> getPage(int limit, int offset) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email\n" +
                "FROM customer\n" +
                "LIMIT ? OFFSET ?";
        Set<Customer> customers = new HashSet<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                customers.add(new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                ));
            }
        } catch (SQLException e) {
            System.out.printf(e.getMessage());
        }
        return customers;

    }

    @Override
    public CustomerCountry getCountryWithMostCustomers() {
        String sql = "SELECT country, count(customer) AS customer_count FROM customer\n" +
                "GROUP BY country\n" +
                "ORDER BY count(country) DESC\n" +
                "LIMIT 1";

        CustomerCountry customerCountry = null;

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                customerCountry = new CustomerCountry(
                        result.getString("country"),
                        result.getInt("customer_count")
                );
            }
        } catch (SQLException e) {
            System.out.printf(e.getMessage());
        }
        return customerCountry;

    }

    @Override
    public CustomerSpender getHighestSpender() {
        String sql = "SELECT c.customer_id, c.first_name, c.last_name, sum(i.total) as amount\n" +
                "FROM invoice AS i\n" +
                "INNER JOIN customer AS c\n" +
                "ON i.customer_id = c.customer_id\n" +
                "GROUP BY c.customer_id \n" +
                "ORDER BY amount DESC \n" +
                "LIMIT 1";

        CustomerSpender spender = null;

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                spender = new CustomerSpender(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getDouble("amount")
                );
            }
        } catch (SQLException e) {
            System.out.printf(e.getMessage());
        }
        return spender;

    }

    @Override
    public Collection<CustomerGenre> getMostPopularGenre(int customerId) {
        String sql = "SELECT i.customer_id, g.name, count(g.name) AS genre_count\n" +
                // Start at invoice, as you can get to track with customer_id
                "FROM invoice AS i\n" +
                // Join with invoice line to get track_id
                "INNER JOIN invoice_line AS il\n" +
                "ON il.invoice_id = i.invoice_id\n" +
                // Join on track to get genre_id
                "INNER JOIN track AS t\n" +
                "ON il.track_id = t.track_id\n" +
                // Join on genre to get name
                "INNER JOIN genre AS g\n" +
                "ON t.genre_id = g.genre_id\n" +
                // Clauses
                "WHERE i.customer_id = ?\n" +
                "GROUP BY g.name, i.customer_id\n" +
                "ORDER BY genre_count DESC\n" +
                "FETCH FIRST 1 ROWS WITH TIES";
        Set<CustomerGenre> customerGenres = new HashSet<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customerId);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                customerGenres.add(new CustomerGenre(
                        result.getInt("customer_id"),
                        result.getString("name"),
                        result.getInt("genre_count")
                ));
            }
        } catch (SQLException e) {
            System.out.printf(e.getMessage());
        }
        return customerGenres;
    }
}
