package no.accelerate.assignment2sample.repositories;

import java.util.Collection;

/**
 * Generic contract for CRUD operations for any record.
 * There is no requirement in this assignment for delete, so I left it out.
 * Normally there would be a delete method too.
 * @param <T> type of the record the repository will wrap.
 * @param <ID> type of the primary key (id) of the record.
 */
public interface CrudRepository <T, ID> {
    Collection<T> findAll();
    T findById(ID id);
    int add(T object);
    int update(T object);
}
