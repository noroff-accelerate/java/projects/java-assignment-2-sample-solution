package no.accelerate.assignment2sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment2SampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment2SampleApplication.class, args);
    }

}
