package no.accelerate.assignment2sample.models;

/**
 * Encapsulation for countries with thr number of customers from them.
 * @param country
 * @param count
 */
public record CustomerCountry(String country,
                              int count) {
}
