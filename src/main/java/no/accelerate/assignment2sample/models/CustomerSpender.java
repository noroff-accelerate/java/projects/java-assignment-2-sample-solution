package no.accelerate.assignment2sample.models;

/**
 * Encapsulation for the amount a customer has spent in total.
 * @param id Customer ID
 * @param firstName
 * @param lastName
 * @param amount Total spent
 */
public record CustomerSpender(int id,
                              String firstName,
                              String lastName,
                              double amount) {
}
