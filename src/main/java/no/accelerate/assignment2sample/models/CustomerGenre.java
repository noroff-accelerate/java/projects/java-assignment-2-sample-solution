package no.accelerate.assignment2sample.models;

/**
 * Encapsulation for a customers most popular genre.
 * Their most popular genre corresponds to the tracks they have purchased.
 * @param id customer ID
 * @param genre
 * @param count how many tracks in the genre purchased by the customer
 */
public record CustomerGenre(int id,
                            String genre,
                            int count) {
}
