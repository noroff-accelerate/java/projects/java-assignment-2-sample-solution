package no.accelerate.assignment2sample.models;

/**
 * Encapsulation for the customer table in the chinook database.
 * @param id
 * @param firstName
 * @param lastName
 * @param country
 * @param postalCode
 * @param phoneNumber
 * @param email
 */
public record Customer(int id,
                       String firstName,
                       String lastName,
                       String country,
                       String postalCode,
                       String phoneNumber,
                       String email) {
}
